## AVルール177
ユーザー定義の変換関数には、単一引数コンストラクタと型変換演算子の2つの形式があります。
暗黙的な型変換は、次のように削除することができます。

* 単一引数コンストラクタ：
単一引数コンストラクタで "explicit"キーワードを使用すると、コンパイラはコンストラクタを通じて暗黙的な変換を行わないようにします。

* 型変換演算子：
変換演算子を定義しません。
型変換機能が必要な場合は、同じ役割を果たすメンバー関数を定義します。
ただし、型変換演算子とは異なり、メンバー関数を明示的に呼び出す必要があり、型変換演算子を使用した場合に発生する可能性のある「驚き」を排除します。

実施例1および2は、これらの原理を示す。

例1a：
以下のVector_intクラスには、ベクトルを構築するために使用される単一の引数コンストラクタがあります。
しかし、このコンストラクタは、ユーザが予期しないように呼び出されることがあります。
解決策は、コンストラクタ宣言で明示的なキーワードを使用して、コンストラクタが暗黙的に呼び出されないようにすることです。

		bool operator == (const Vector_int &lhs, const Vector_int &rhs)
		{
		  // compare two Vector_ints
		}

		class Vector_int {
		  public:
		    Vector_int (int32 n);
		    ...
		};

		Vector_int v1(10),
		           v2(10);   // create two vectors of size 10;
		...
		for (int32 i=0 ; i<10 ; ++i)
		{
		  if (v1 == v2[i])   // The programmer meant to compare the elements of two Vectors.
		  {                  // However, the subscript of the first was inadvertently left off.
		    ...              // Thus, the compiler is asked to compare a Vector_int with an
		  }                  // integer. The single argument constructor is called to convert the
		}                    // integer to a new Vector_int so that the comparison can take place.
		                     // This is almost certainly not what is expected. 

例1b：
コンストラクタは明示的に宣言され、コンパイル時にエラーがキャッチされます。

		class Vector_int
		{
		  public:
		    explicit Vector_int (int32 n) ;
		    ...
		};

		Vector_int v1(10), v2(10); // create two vectors of size 10;
		...
		for (int32 i=0 ; i<10 ; ++i)
		{
		  if (v1 == v2[i])         // The programmer meant to compare the elements of two Vectors.
		  {                        // However, the subscript of the first was inadvertently left off.
		    ...                    // Thus, the compiler is asked to compare a Vector_int with an
		  }                        // integer. The explicit keyword prevents the constructor from
		}                          // being called implicitly, so the compiler generates an error. 

例2a：
クラス複合体は複素数を定義しますが、出力演算子はそのクラスに対して定義されていません。
したがって、ユーザーが複素数を印刷しようとすると、エラーは生成されません。
代わりに、番号は変換演算子によって暗黙的に実数に変換されます。
これは、クライアントに潜在的に驚くべき結果をもたらす。

		class Complex
		{
		  public:
		    Complex (double r, double i = 1) : real(r), imaginary(i) {} // Constructor
		    operator double() const;                                    // Conversion operator
		    ...                                                         // converts Complex to double
		  private:
		    double real;
		    double imaginary;
		};

		Complex r(1,2);
		
		cout << r << endl; // User might expect compile error, but instead
		                   // r is automatically converted to decimal form
		                   // potentially losing information. 

例2b：
変換演算子の代わりに、クラス複合には同じ役割を果たすメンバー関数が追加されました。
したがって、同じ機能が維持されますが、潜在的な驚きはありません。

		class Complex
		{
		  public:
		    Complex (double r, double i = 1) : real(r), imaginary(i) {} // Constructor
		    double as_double() const;                                   // Conversion operator
		    ...                                                         // converts Complex to double
		  private:
		    double real;
		    double imaginary;
		};

		Complex r(1,2);
		
		cout << r << endl;            // Compile error generated.
		cout << r.asDouble() << endl; // Called explicitly rather than
		                              // implicitly. 


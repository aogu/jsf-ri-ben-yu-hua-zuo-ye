## AVルール160
このルールの目的は、あいまいであるか、または他の点で誤解されやすい状況での割り当てを禁止することです。
次の例は、このルールが解決するいくつかの問題を示しています。

注意：
for-init-statement（宣言ではない）は式文です。

		for ( for-init-statement condition-opt ; expression-opt ) statement

		for-init-statement:
			expression-statement
			simple-declaration

例：

		x = y;                  // Good: the intent to assign y to x and then check if x is
		if (x != 0)             // not zero is explicitly stated.
		{
		  foo ();
		}

		if ( ( x = y) != 0 )    // Bad: not as readable as it could be.
		{                       // Assignment should be performed prior to the “if”statement
		  foo ();
		}

		if (x = y)              // Bad: intent is very obscure: a code reviewer could easily
		{                       // think that “==” was intended instead of “=”.
		  foo ();
		}

		for (i=0 ; i<max ; ++i) // Good: assignment in expression statement of “for” statement
		{
		  ...
		} 


## AVルール137
MISRA理由：
ファイルスコープの宣言は、デフォルトで外部にあります。
したがって、2つのファイルの両方がファイルスコープで同じ名前の識別子を宣言すると、リンカはエラーを返すか、プログラマが意図したものとは異なる可能性がある同じ変数になります。
変数のいずれかがライブラリのどこかにある場合も同様です。
静的なstorageclass指定子を使用することにより、識別子が宣言されているファイルにのみ表示されるようになります。

変数が同じファイル内の関数によってのみ使用される場合は、staticを使用します。
同様に、関数が同じファイル内の他の場所からのみ呼び出される場合は、staticを使用します。

通常、ヘッダー（.h）ファイルに宣言が含まれる関数は、他のファイルから呼び出されることを想定しているため、staticキーワードでは決して指定しないでください。
逆に、実装本体（.cpp）ファイルに宣言が含まれる関数は、他のファイルから決して呼び出すべきではないので、常にstaticキーワードで宣言する必要があります。


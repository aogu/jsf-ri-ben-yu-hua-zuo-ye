## AVルール59
次の例が示すように、if、else if、else、while、do..while、およびforステートメントの本文は、常に中括弧で囲む必要があります。
例Aに示すように、後で追加されるコードは、中括弧で囲まれていない限り、ブロックの一部ではありません。
さらに、例Bに示すように、「;」は単独で見るのが難しい場合があります。
したがって、制御フロープリミティブの後にブロック（空であっても）が必要です。

例A：
---
		if (flag == 1)
		{
		  success ();
		}
		else                    // Incorrect: log_error() was added at a later time
		  clean_up_resources(); // but is not part of the block (even though
		  log_error();          // it is at the proper indentation level). 

例B：空であっても、制御フロープリミティブの後にブロックが必要です。
---
		while (f(x)); // Incorrect: “;” is difficult to see.
		while (f(x))  // Incorrect: “;” is difficult to see.
		  ;
		while (f(x))  // Correct
		{
		} 


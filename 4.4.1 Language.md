## AVルール8 (shall)

すべてのコードは、ISO / IEC 14882：2002（E）標準C++に準拠するものとする。 [10]

理論的根拠：ISO / IEC 14882は、C++プログラミング言語を定義する国際標準です。
したがって、すべてのコードは、ISO / IEC 14882に関して明確に定義されなければならない。
ISO / IEC 14882の言語拡張またはバリエーションは許されない。


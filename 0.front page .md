## 原文

JOINT STRIKE FIGHTER  
AIR VEHICLE  
C++ CODING STANDARDS  
FOR THE SYSTEM DEVELOPMENT AND DEMONSTRATION PROGRAM  
Document Number 2RDU00001 Rev C  
December 2005  

Copyright 2005 by Lockheed Martin Corporation.  
DISTRIBUTION STATEMENT A: Approved for public release; distribution is unlimited.  

***

## 日本語訳

統合打撃戦闘機  
航空機  
C++コーディング規格  
システム開発とデモンストレーションプログラムのために  
文書番号2RDU00001 Rev C  
  
2005年12月  

Lockheed Martin Corporationによる著作権2005。  
配布声明A：一般公開のために承認された。 配布は無制限です。  

***

日本語訳は参考のために作成しました。
正確な情報は原文を参照してください。

***


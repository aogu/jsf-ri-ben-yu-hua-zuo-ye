## AVルール151.1
文字列リテラルは定数なので、以下に示すように、定数ポインタにのみ割り当てる必要があります。

		char* c1 = “Hello”;         // Bad: assigned to non-const
		char c2[] = “Hello”;        // Bad: assigned to non-const
		char c3[6] = “Hello”;       // Bad: assigned to non-const
		c1[3] = ‘a’;                // Undefined (but compiles)
		const char* c1 = “Hello”;   // Good
		const char c2[] = “Hello”;  // Good
		const char c3[6] = “Hello”; // Good
		c1[3] = ‘a’;                // Compile error 


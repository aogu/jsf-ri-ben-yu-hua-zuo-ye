## AVルール51 (will)
関数名と変数名に含まれるすべての文字は、完全に小文字で構成されます。

理論的根拠：
スタイル。

例：

		class Example_class_name
		{
			public:
				uint16 example_function_name (void);
			private:
				uint16 example_variable_name;
		};


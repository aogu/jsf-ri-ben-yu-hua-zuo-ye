## AVルール94
非仮想関数は静的にバインドされています。
本質的に、非仮想関数は対応する基本クラスのバージョンを隠すでしょう。
したがって、単一の派生クラスオブジェクトは、ベースクラスポインタ/参照または派生クラスポインタ/参照を介してアクセスされた方法に応じて、ベースクラスオブジェクトまたは派生クラスオブジェクトのいずれかとして動作することができる。
このような振る舞いの二重性を避けるために、非仮想関数は決して再定義されるべきではありません。

例：

		class Base
		{
		  public:
		    mf (void);
		};

		class Derived : public Base
		{
		  public:
		    mf (void);
		};

		example_function(void)
		{
		  Derived derived;
		  Base* base_ptr = &derived;       // Points to derived
		  Derived* derived_ptr = &derived; // Points to derived
		  base_ptr->mf();                  // Calls Base::mf() *** Different behavior for same object!!
		  derived_ptr->mf();               // Calls Derived::mf()
		} 


## AVルール145
列挙子リストがメンバーの明示的な初期化なしで与えられた場合、C++は、最初の要素には0から始まり、後続の要素ごとに1ずつ増加する整数のシーケンスを割り当てます。
ほとんどの場合、これで十分でしょう。

上記の規則で許可されているように、最初の要素を明示的に初期化すると、整数の割り当てが強制的に与えられた値で開始されます。
このアプローチを採用する場合、使用される初期化値がリストの後続の値が列挙定数によって使用されるint記憶域を超えないように十分に小さいことを確認することが不可欠です。

リスト内のすべての項目を明示的に初期化することは許容されますが、自動割り当てと手動割り当てが混在することはありません。
これはエラーが発生しやすくなります。
しかし、プログラマは、すべての値が必要な範囲内にあり、値が意図せずに複製されないようにする責任があります。

例A：

		//Legal enumerated list using compiler-assigned enum values
		//off=0, green=1, yellow=2, red=3

		enum Signal_light_states_type
		{
		  off,
		  green,
		  yellow,
		  red
		};

例2：

		// Legal enumeration, assigning a value to the first item in the list.

		enum Channel_assigned_type
		{
		  channel_unassigned = -1,
		  channel_a,
		  channel_b,
		  channel_c
		};

例3：

		// Control mask enumerated list. All items explicitly
		// initialized.

		enum FSM_a_to_d_control_enum_type
		{
		  start_conversion = 0x01,
		  stop_conversion = 0x02,
		  start_list = 0x04,
		  end_list = 0x08,
		  reserved_3_bit = 0x70,
		  reset_device = 0x80
		}; 

例4:

		// Legal: standard convention used for enumerations that are intended to index arrays.

		enum Color {
		  red,
		  orange,
		  yellow,
		  green,
		  blue,
		  indigo,
		  violet,
		  Color_begin = red,
		  Color_end = violet,
		  Color_NOE // Number of elements in array
		};


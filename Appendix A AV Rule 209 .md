## AVルール209
UniversalTypesファイルは、開発者が使用するすべての標準タイプを定義するために作成されます。
タイプには次のものがあります。

		bool,                           // built-in type
		char,                           // built-in type
		int8, int16, int32, int64,      // user-defined types
		uint8, uint16, uint32, uint64,  // user-defined types
		float32, float64                // user-defined types 

注：
charが符号付きまたは符号なしの値を表すかどうかは、実装定義です。
しかし、現代の実装ではcharをunsigned charとして扱うので、組み込みのchar型は符号なしであると仮定して使用されます。


## AVルール108
次の例は、不特定多数の引数の代わりに関数のオーバーロードまたはパラメータのデフォルトが使用される場合を示しています。

例A：
2次元ベクトル、3次元ベクトル、または4次元ベクトルの長さを計算する関数を考えてみましょう。
可変引数リストを使用することもできますが、不要な複雑さが生じます。
あるいは、関数のオーバーロードまたはパラメータのデフォルト設定により、より良い解決策が提供されます。

Unspecified number of arguments
---
		float32 vector_length (float32 x, float32 y, …); // Error prone

Function overloading
---
		float32 vector_length (float32 x, float32 y);
		float32 vector_length (float32 x, float32 y, float32 z);
		float32 vector_length (float32 x, float32 y, float32, z, float32 w);

Default parameters
---
		float32 vector_length (float32 x, float32 y, float32 z=0, float32 w=0);


## AVルール136
次のコードは、変数が実現可能な最小スコープで宣言されていない場合に発生するいくつかの問題を示しています。

		void fun_1()
		{
		  int32 i;                     // Bad: i is prematurely declared (the intent is to use i in the
		                               // for loop only)
		  ...                          // Bad: i has a meaningless value in the region of the code
		  for (i=0 ; i<max ; ++i)
		  {
		    ...
		  }
		  ...                          // Bad: i should not be used here, but could be used anyway

		  for(int32 j=0 ; j<max ; ++j) // Good: j is not declared or initialized until needed
		  {                            // Good: j is only known within the for loop’s scope
		    ...
		  }
		}


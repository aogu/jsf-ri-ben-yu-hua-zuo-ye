## AVルール187
ISO / IEC 14882では、実行環境の状態の変化として副作用が定義されています。
より正確に、

揮発性の左辺値によって指定されたオブジェクトにアクセスする、オブジェクトを変更する、ライブラリI / O関数を呼び出す、またはこれらの操作のいずれかを行う関数を呼び出すことは、実行環境の状態の変化である副作用です。

例：
潜在的な副作用

		if (flag) // Has side effect only if flag is true.
		{
		  foo();
		} 

例：
次の式は副作用がありません

		3 + 4; // Bad: statement has zero side effects 

例：
次の式には副作用があります

		x = 3 + 4;  // Statement has one side effect: x is set to 7.
		y = x++;    // Statement two side effects: y is set to x and x is incremented.


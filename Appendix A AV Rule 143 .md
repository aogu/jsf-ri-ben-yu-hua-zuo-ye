## AVルール143
意味のある値を割り当てる前に変数を導入すると、次の例に示すように多くの問題が発生します。

例A：
以下のコードは、変数が適切に初期化される前に導入されたときに発生したいくつかの問題を示しています。

		void fun_1()   // Poor implementation
		{
		  int32 i;     // Bad: i is prematurely declared (the intent is to use i in the for
		               // loop only)
		  int32 max=0; // Bad: max initialized with a dummy value.
		  ...          // Bad: i and max have meaningless values in this
		               // region of the code.
		  max = f(x);
		  for (i=0 ; i<max ; ++i)
		  {
		    ...
		  }
		  ...          // Bad: i should not be used here, but could be used anyway
		}

		void fun_1()                    // Good implementation
		{
		  ...
		  int32 max = f(x);             // Good: max not introduced until meaningful value is
		                                // available
		  for (int32 i=0 ; i<max ; ++i) // Good: i is not declared or initialized until needed
		  {                             // Good: i is only known within the for loop’s scope
		    ...
		  }
		}

例B：
クラスXのインスタンスは、完全に初期化できる点の前に構築されています。
初期化を完了するには、十分な情報が利用可能になったときに別のinit（）メソッドを呼び出さなければなりません。
しかし、オブジェクトはinit（）の呼び出しに先立つ準双曲状態にしかないので、オブジェクト構築とinit（）の間のすべてのメソッド呼び出しは疑わしいものです。
不必要なデフォルトのコンストラクタに関するAVルール73も参照してください。

		class X {
		  public:
		    X::X() {} // Bad: default constructor builds partially initialized object.
		    init (int32 max_, int32 min_)
		    {
		      max = _max ;
		      min = _min;
		    }
		    int32 range()
		    {
		      return max-min ;
		    }
		    ...
		  private:
		    int32 max;
		    int32 min;
		};

		void foo()
		{
		  X x;                    // Bad: x constructed but without data
		  ...
		  x.range();              // Bad: undefined result.
		  ...
		  x.init(lbound, ubound); // Bad: x initialized later than necessary
		} 


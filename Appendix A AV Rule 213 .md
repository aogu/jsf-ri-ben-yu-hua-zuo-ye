## AVルール213
読みやすさを向上させ、間違いを減らすために、演算子の優先順位ルールを明確にするために、かっこを使用する必要があります。
しかし、括弧を過度に使用すると式が乱雑になって可読性が低下する可能性があります。
算術演算子の下のかっこを必要とすると、読みやすさとクラッタの間で妥当なバランスが取られます。

表2は、表の上位の項目が表の下位の項目よりも優先順位が高いC++の演算子優先ルールを示しています。

訳注：
表2はオリジナル文書137,138ページを参照。

例：
次の例を考えてみましょう。
括弧は算術演算子以下の演算子の演算子順序を指定する必要があることに注意してください。

		x = a * b + c;                // Good: can assume “*” binds before “+”
		x = v->a + v->b + w.c;        // Good: can assume “->” and “.” Bind before “+”

		x = (f()) + ((g()) * (h()));  // Bad: overuse of parentheses. Can assume
		                              // function call binds before “+” and “*”
		x = a & b | c;                // Bad: must use parenthesis to clarify order
		x = a >> 1 + b;               // Bad: must use parenthesis to clarify order 


1. ANSI / IEEE Std 754、1985年のバイナリ浮動小数点演算のIEEE標準。
2. Bjarne Stroustrup。 C++プログラミング言語、第3版。 Addison-Wesley、2000。
3. Bjarne Stroustrup。 Bjarne StroustrupのC++用語集です。
4. Bjarne Stroustrup。 Bjarne StroustrupのC++スタイルとテクニックのFAQ
5. Barbara Liskov。データ抽象化と階層、SIGPLAN Notices、23、5（1988年5月）
6. スコットマイヤー。効果的なC++：あなたのプログラムとデザインを改善する50の具体的な方法、第2版。 Addison-Wesley、1998年。
7. スコット・マイヤーズ。より効果的なC++：プログラムとデザインを改善する35の新しい方法Addison-Wesley、1996。
8. 自動車産業ソフトウェア信頼性協会。車両ベースのソフトウェアにおけるC言語の使用に関するガイドライン、1998年4月。
9. ISO / IEC 10646-1、情報技術 - ユニバーサルマルチオクテッド符号化文字集合（UCS） - 第1部：アーキテクチャと基本多言語面、1993。
10. ISO / IEC 14882：2003（E）、プログラミング言語 - C++。アメリカ国立標準研究所、ニューヨーク、ニューヨーク10036,2003。
11. ISO / IEC 9899：1990、プログラミング言語 - C、ISO、1990
12. JSFミッションシステムソフトウェア開発計画。
13. JSFシステム安全プログラム計画。 DOC。 No. 2YZA00045-0002。
14. C++規則と勧告でのプログラミング。 Copyright©Ellemtel Telecommunication Systems Laboratories、Box 1505、125 25 Alvsjo、Sweden文書：M 90 0118 Uen、Rev. C、1992年4月27日。以下の声明で許可されて使用されています。この完全な著作権および許可の通知がすべてのコピーで損なわれない限り、この文書を複製、改変および頒布することはできません。
15. RTCA / DO-178B、航空機搭載システムおよび機器認証におけるソフトウェア検討、1992年12月。

訳注：
以下はオリジナルの文です。

1. ANSI/IEEE Std 754, IEEE Standard for Binary Floating-Point Arithmetic, 1985.
2. Bjarne Stroustrup. The C++ Programming Language, 3rd Edition. Addison-Wesley, 2000.
3. Bjarne Stroustrup. Bjarne Stroustrup's C++ Glossary.
4. Bjarne Stroustrup. Bjarne Stroustrup's C++ Style and Technique FAQ.
5. Barbara Liskov. Data Abstraction and Hierarchy, SIGPLAN Notices, 23, 5 (May, 1988).
6. Scott Meyers. Effective C++: 50 Specific Ways to Improve Your Programs and Design, 2nd Edition. Addison-Wesley, 1998.
7. Scott Meyers. More Effective C++: 35 New Ways to Improve Your Programs and Designs. Addison-Wesley, 1996.
8. Motor Industry Software Reliability Association. Guidelines for the Use of the C Language in Vehicle Based Software, April 1998.
9. ISO/IEC 10646-1, Information technology - Universal Multiple-Octet Coded Character Set (UCS) - Part 1: Architecture and Basic Multilingual Plane, 1993.
10. ISO/IEC 14882:2003(E), Programming Languages – C++. American National Standards Institute, New York, New York 10036, 2003.
11. ISO/IEC 9899: 1990, Programming languages - C, ISO, 1990.
12. JSF Mission Systems Software Development Plan.
13. JSF System Safety Program Plan. DOC. No. 2YZA00045-0002.
14. Programming in C++ Rules and Recommendations. Copyright © by Ellemtel Telecommunication Systems Laboratories Box 1505, 125 25 Alvsjo, Sweden Document: M 90 0118 Uen, Rev. C, 27 April 1992. Used with permission supplied via the following statement: Permission is granted to any individual or institution to use, copy, modify and distribute this document, provided that this complete copyright and permission notice is maintained intact in all copies. 
15. RTCA/DO-178B, Software Considerations in Airborne Systems and Equipment Certification, December 1992. 


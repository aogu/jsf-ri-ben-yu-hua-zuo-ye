## AVルール79
デストラクタのリソースを解放すると、特に例外的なケースに関して、リソース管理の便利な手段が提供されます。
さらに、リソースがリークする可能性がある場合、そのリソースはデストラクタがリソースを自動的にクリーンアップするクラスにラップする必要があります。

例A：
Stroustrup [2]は、ファイルハンドルに基づく例を提供します。
デストラクタがファイルを閉じている間、コンストラクタはファイルを開きます。
クライアントがリソースをクリーンアップするのを "忘れる"可能性はなくなります。

		class File_ptr        // Raw file pointer wrapped in class to ensure
		{                     // resources are not leaked.
		  public:
		    File_ptr (const char *n, const char * a) { p = fopen(n,a); }
		    File_ptr (FILE* pp) { p = pp; }

		    ~File_ptr ()
		    {
		      if (p)
		      {
		        fclose(p)
		      };
		    }                 // Clean up file handle.
		    ...
		  private:
		    FILE *p;
		};

		use_file (const char *file_name)
		{                     // Client does not have to remember to clean up file handle
		  File_ptr f(fn,”r”); // (impossible to leak file handles).
		                      // use f
		}                     // f goes out of scope so the destructor is called,
		                      // cleaning up the file handle.


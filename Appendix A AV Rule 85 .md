## AVルール85
次の例は、operator ==（）がoperator ==（）によって定義される方法を示しています。
この構成により、保守が簡単になります。

		bool operator==(Sometype a)
		{
		  if ( (a.attribute_1 == attribute_1) &&
		       (a.attribute_2 == attribute_2) &&
		       (a.attribute_3 == attribute_3) &&
		       ...
		       (a.attribute_n == attribute_n) )
		  {
		    return true;
		  }
		  else
		  {
		    return false;
		  }
		}

		bool operator!=(Some_type a)
		{
		  return !(*this==a); //Note “!=” is defined in terms of "=="
		} 


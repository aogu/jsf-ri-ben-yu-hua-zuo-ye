
## [1 INTRODUCTION](1 INTRODUCTION.md)

## [2 REFERENCED DOCUMENTS](2 REFERENCED DOCUMENTS.md)

## [3 GENERAL DESIGN](3 GENERAL DESIGN.md)

### [3.1 Coupling & Cohesion](3.1 Coupling & Cohesion.md)

### [3.2 Code Size and Complexity](3.2 Code Size and Complexity.md)

ルール1-3

## [4 C++ CODING STANDARDS](4 C++ CODING STANDARDS.md)

### [4.1 Introduction](4.1 Introduction.md)

### [4.2 Rules](4.2 Rules.md)

#### [4.2.1 Should, Will, and Shall Rules](4.2.1 Should, Will, and Shall Rules.md)

#### [4.2.2 Breaking Rules](4.2.2 Breaking Rules.md)

ルール4-6

#### [4.2.3 Exceptions to Rules](4.2.3 Exceptions to Rules.md)

ルール7

### [4.3 Terminology](4.3 Terminology.md)

### [4.4 Environment](4.4 Environment.md)

#### [4.4.1 Language](4.4.1 Language.md)

ルール8

#### [4.4.2 Character Sets](4.4.2 Character Sets.md)

ルール9-14

#### [4.4.3 Run-Time Checks](4.4.3 Run-Time Checks.md)

ルール15

### [4.5 Libraries](4.5 Libraries.md)

ルール16

#### [4.5.1 Standard Libraries](4.5.1 Standard Libraries.md)

ルール17-25

### [4.6 Pre-Processing Directives](4.6 Pre-Processing Directives.md)

ルール26

#### [4.6.1 #ifndef and #endif Pre-Processing Directives](4.6.1 #ifndef and #endif Pre-Processing Directives.md)

ルール27-28

#### [4.6.2 #define Pre-Processing Directive](4.6.2 #define Pre-Processing Directive.md)

ルール29-31

#### [4.6.3 #include Pre-Processing Directive](4.6.3 #include Pre-Processing Directive.md)

ルール32

### [4.7 Header Files](4.7 Header Files.md)

ルール33-39

### [4.8 Implementation Files](4.8 Implementation Files.md)

ルール40

### [4.9 Style](4.9 Style.md)

ルール41-44

#### [4.9.1 Naming Identifiers](4.9.1 Naming Identifiers.md)

ルール45-49

##### [4.9.1.1 Naming Classes, Structures, Enumerated types and typedefs](4.9.1.1 Naming Classes, Structures, Enumerated types and typedefs.md)

ルール50

##### [4.9.1.2 Naming Functions, Variables and Parameters](4.9.1.2 Naming Functions, Variables and Parameters.md)

ルール51

##### [4.9.1.3 Naming Constants and Enumerators](4.9.1.3 Naming Constants and Enumerators.md)

ルール52

#### [4.9.2 Naming Files](4.9.2 Naming Files.md)

ルール53-56

#### [4.9.3 Classes](4.9.3 Classes.md)

ルール57

#### [4.9.4 Functions](4.9.4 Functions.md)

ルール58

#### [4.9.5 Blocks](4.9.5 Blocks.md)

ルール59-61

#### [4.9.6 Pointers and References](4.9.6 Pointers and References.md)

ルール62

#### [4.9.7 Miscellaneous](4.9.7 Miscellaneous.md)

ルール63

### [4.10 Classes](4.10 Classes.md)

#### [4.10.1 Class Interfaces](4.10.1 Class Interfaces.md)

ルール64

#### [4.10.2 Considerations Regarding Access Rights](4.10.2 Considerations Regarding Access Rights.md)

ルール65-67

#### [4.10.3 Member Functions](4.10.3 Member Functions.md)

ルール68

#### [4.10.4 const Member Functions](4.10.4 const Member Functions.md)

ルール69

#### [4.10.5 Friends](4.10.5 Friends.md)

ルール70

#### [4.10.6 Object Lifetime, Constructors, and Destructors](4.10.6 Object Lifetime, Constructors, and Destructors.md)

##### [4.10.6.1 Object Lifetime](4.10.6.1 Object Lifetime.md)

ルール70.1

##### [4.10.6.2 Constructors](4.10.6.2 Constructors.md)

ルール71-77.1

##### [4.10.6.3 Destructors](4.10.6.3 Destructors.md)

ルール78-79

#### [4.10.7 Assignment Operators](4.10.7 Assignment Operators.md)

ルール80-83

#### [4.10.8 Operator Overloading](4.10.8 Operator Overloading.md)

ルール84-85

#### [4.10.9 Inheritance](4.10.9 Inheritance.md)

ルール86-97

#### [4.10.10Virtual Member Functions](4.10.10Virtual Member Functions.md)

ルール97.1

### [4.11 Namespaces](4.11 Namespaces.md)

ルール98-100

### [4.12 Templates](4.12 Templates.md)

ルール101-106

### [4.13 Functions](4.13 Functions.md)

#### [4.13.1 Function Declaration, Definition and Arguments](4.13.1 Function Declaration, Definition and Arguments.md)

ルール107-112

#### [4.13.2 Return Types and Values](4.13.2 Return Types and Values.md)

ルール113-115

#### [4.13.3 Function Parameters (Value, Pointer or Reference)](4.13.3 Function Parameters (Value, Pointer or Reference).md)

ルール116-118.2

#### [4.13.4 Function Invocation](4.13.4 Function Invocation.md)

ルール119

#### [4.13.5 Function Overloading](4.13.5 Function Overloading.md)

ルール120

#### [4.13.6 Inline Functions](4.13.6 Inline Functions.md)

ルール121-124

#### [4.13.7 Temporary Objects](4.13.7 Temporary Objects.md)

ルール125

### [4.14 Comments](4.14 Comments.md)

ルール126-134

### [4.15 Declarations and Definitions](4.15 Declarations and Definitions.md)

ルール135-141

### [4.16 Initialization](4.16 Initialization.md)

ルール142-145

### [4.17 Types](4.17 Types.md)

ルール146-148

### [4.18 Constants](4.18 Constants.md)

ルール149-151.1

### [4.19 Variables](4.19 Variables.md)

ルール152

### [4.20 Unions and Bit Fields](4.20 Unions and Bit Fields.md)

ルール153-156

### [4.21 Operators](4.21 Operators.md)

ルール157-168

### [4.22 Pointers & References](4.22 Pointers & References.md)

ルール169-176

### [4.23 Type Conversions](4.23 Type Conversions.md)

ルール177-185

### [4.24 Flow Control Structures](4.24 Flow Control Structures.md)

ルール186-201

### [4.25 Expressions](4.25 Expressions.md)

ルール202-205

### [4.26 Memory Allocation](4.26 Memory Allocation.md)

ルール206-207

### [4.27 Fault Handling](4.27 Fault Handling.md)

ルール208

### [4.28 Portable Code](4.28 Portable Code.md)

#### [4.28.1 Data Abstraction](4.28.1 Data Abstraction.md)

ルール209

#### [4.28.2 Data Representation](4.28.2 Data Representation.md)

ルール210-211

#### [4.28.3 Underflow Overflow](4.28.3 Underflow Overflow.md)

ルール212

#### [4.28.4 Order of Execution](4.28.4 Order of Execution.md)

ルール213-214

#### [4.28.5 Pointer Arithmetic](4.28.5 Pointer Arithmetic.md)

ルール215

### [4.29 Efficiency Considerations](4.29 Efficiency Considerations.md)

ルール216

### [4.30 Miscellaneous](4.30 Miscellaneous.md)

ルール217-218

## [5 TESTING](5 TESTING.md)

#### [5.1.1 Subtypes](5.1.1 Subtypes.md)

ルール219

#### [5.1.2 Structure](5.1.2 Structure.md)

ルール220-221

### [Appendix A AV Rule 3](Appendix A AV Rule 3 .md)

### [Appendix A AV Rule 11](Appendix A AV Rule 11 .md)

### [Appendix A AV Rule 12](Appendix A AV Rule 12 .md)

### [Appendix A AV Rule 15](Appendix A AV Rule 15 .md)

### [Appendix A AV Rule 29](Appendix A AV Rule 29 .md)

### [Appendix A AV Rule 30](Appendix A AV Rule 30 .md)

### [Appendix A AV Rule 32](Appendix A AV Rule 32 .md)

### [Appendix A AV Rule 36](Appendix A AV Rule 36 .md)

### [Appendix A AV Rule 38](Appendix A AV Rule 38 .md)

### [Appendix A AV Rule 39](Appendix A AV Rule 39 .md)

### [Appendix A AV Rule 40](Appendix A AV Rule 40 .md)

### [Appendix A AV Rule 42](Appendix A AV Rule 42 .md)

### [Appendix A AV Rule 58](Appendix A AV Rule 58 .md)

### [Appendix A AV Rule 59](Appendix A AV Rule 59 .md)

### [Appendix A AV Rule 70](Appendix A AV Rule 70 .md)

### [Appendix A AV Rule 70.1](Appendix A AV Rule 70.1.md)

### [Appendix A AV Rule 71](Appendix A AV Rule 71 .md)

### [Appendix A AV Rule 71.1](Appendix A AV Rule 71.1 .md)

### [Appendix A AV Rule 73](Appendix A AV Rule 73 .md)

### [Appendix A AV Rule 74](Appendix A AV Rule 74 .md)

### [Appendix A AV Rule 76](Appendix A AV Rule 76 .md)

### [Appendix A AV Rule 77](Appendix A AV Rule 77 .md)

### [Appendix A AV Rule 77.1](Appendix A AV Rule 77.1 .md)

### [Appendix A AV Rule 79](Appendix A AV Rule 79 .md)

### [Appendix A AV Rule 81](Appendix A AV Rule 81 .md)

### [Appendix A AV Rule 83](Appendix A AV Rule 83 .md)

### [Appendix A AV Rule 85](Appendix A AV Rule 85 .md)

### [Appendix A AV Rule 87](Appendix A AV Rule 87 .md)

### [Appendix A AV Rule 88](Appendix A AV Rule 88 .md)

### [Appendix A AV Rule 88.1](Appendix A AV Rule 88.1 .md)

### [Appendix A AV Rule 92](Appendix A AV Rule 92 .md)

### [Appendix A AV Rule 93](Appendix A AV Rule 93 .md)

### [Appendix A AV Rule 94](Appendix A AV Rule 94 .md)

### [Appendix A AV Rule 95](Appendix A AV Rule 95 .md)

### [Appendix A AV Rule 101 and AV Rule 102](Appendix A AV Rule 101 and AV Rule 102 .md)

### [Appendix A AV Rule 103](Appendix A AV Rule 103 .md)

### [Appendix A AV Rule 108](Appendix A AV Rule 108 .md)

### [Appendix A AV Rule 109](Appendix A AV Rule 109 .md)

### [Appendix A AV Rule 112](Appendix A AV Rule 112 .md)

### [Appendix A AV Rule 120](Appendix A AV Rule 120 .md)

### [Appendix A AV Rule 121](Appendix A AV Rule 121 .md)

### [Appendix A AV Rule 122](Appendix A AV Rule 122 .md)

### [Appendix A AV Rule 124](Appendix A AV Rule 124 .md)

### [Appendix A AV Rule 125](Appendix A AV Rule 125 .md)

### [Appendix A AV Rule 126](Appendix A AV Rule 126 .md)

### [Appendix A AV Rule 136](Appendix A AV Rule 136 .md)

### [Appendix A AV Rule 137](Appendix A AV Rule 137 .md)

### [Appendix A AV Rule 138](Appendix A AV Rule 138 .md)

### [Appendix A AV Rule 139](Appendix A AV Rule 139 .md)

### [Appendix A AV Rule 141](Appendix A AV Rule 141 .md)

### [Appendix A AV Rule 142](Appendix A AV Rule 142 .md)

### [Appendix A AV Rule 143](Appendix A AV Rule 143 .md)

### [Appendix A AV Rule 145](Appendix A AV Rule 145 .md)

### [Appendix A AV Rule 147](Appendix A AV Rule 147 .md)

### [Appendix A AV Rule 151.1](Appendix A AV Rule 151.1 .md)

### [Appendix A AV Rule 157](Appendix A AV Rule 157 .md)

### [Appendix A AV Rule 158](Appendix A AV Rule 158 .md)

### [Appendix A AV Rule 160](Appendix A AV Rule 160 .md)

### [Appendix A AV Rule 168](Appendix A AV Rule 168 .md)

### [Appendix A AV Rule 177](Appendix A AV Rule 177 .md)

### [Appendix A AV Rule 180](Appendix A AV Rule 180 .md)

### [Appendix A AV Rule 185](Appendix A AV Rule 185 .md)

### [Appendix A AV Rule 187](Appendix A AV Rule 187 .md)

### [Appendix A AV Rule 192](Appendix A AV Rule 192 .md)

### [Appendix A AV Rule 193](Appendix A AV Rule 193 .md)

### [Appendix A AV Rule 204](Appendix A AV Rule 204 .md)

### [Appendix A AV Rule 204.1](Appendix A AV Rule 204.1 .md)

### [Appendix A AV Rule 207](Appendix A AV Rule 207 .md)

### [Appendix A AV Rule 209](Appendix A AV Rule 209 .md)

### [Appendix A AV Rule 210.1](Appendix A AV Rule 210.1 .md)

### [Appendix A AV Rule 213](Appendix A AV Rule 213 .md)

### [Appendix A AV Rule 214](Appendix A AV Rule 214 .md)

### [Appendix A AV Rule 215](Appendix A AV Rule 215 .md)

### [Appendix A AV Rule 216](Appendix A AV Rule 216 .md)

### [Appendix A AV Rule 220](Appendix A AV Rule 220 .md)

### [Appendix B](Appendix B .md)


## AVルール30
const変数はスコープルールに従い、型チェックの対象となり、テキスト置換を必要としないので（混乱したり誤解を招く可能性がある）、次の例に示すようにマクロよりも好ましいです。

例：

		#define max_count 100        // Wrong: no type checking
		const int16 max_count = 100; // Correct: type checking may be performed 

注：
積分定数はオプティマイザで除去できますが、非整数定数では削除できません。 したがって、上記の例では、max_countは結果の画像に表示されません。


## AVルール32
このルールの例外として、テンプレートクラスと関数定義があります。
これらの定義は、別々のヘッダファイルと実装ファイルに分割できます。
この場合、実装ファイルはヘッダファイルの一部として含めることができます。
実装ファイルは論理的にヘッダの一部であり、以下に示すように個別にコンパイルすることはできません。

例：

File A.h
		--------------------------------
		#ifndef A_H
		#define A_H
		template< class T >
		class A
		{
		public:
		  void do_something();
		};
		#include <A.cpp>
		#endif
		-------------------------------

File A.cpp
		-------------------------------
		template< class T >
		A<T>::do_something()
		{
		  // do_something impelemtation
		}
		------------------------------- 


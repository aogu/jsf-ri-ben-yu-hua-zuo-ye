## AVルール15（MISRA規則4、改訂版）(shall)

ランタイムチェック（防御プログラミング）のための準備がなされなければならない。

理論的根拠：
SEAL 1またはSEAL 2ソフトウェア[13]については、ソフトウェアおよびシステム機能の適切な動作を保証するための規定がなされなければならない。
詳細については、[付録AのAVルール15](Appendix A AV Rule 15 .md)を参照してください。


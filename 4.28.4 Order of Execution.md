## AVルール213（MISRA規則47、改訂版） (shall)
演算子の下の式の中で、C++の演算子優先順位規則に依存するものはありません。

根拠：
読みやすさ。
詳細については、[付録AのAVルール213](Appendix A AV Rule 213 .md)を参照してください。

MISRA規則47はshouldをshallと置き換えることで変更されました。

## AVルール214
別々の翻訳単位で非局所的な静的オブジェクトが特別な順序で初期化されてはならないと仮定します。

理論的根拠：
注文の依存関係はバグを見つけにくい。
詳細については、[付録AのAVルール214](Appendix A AV Rule 214 .md)を参照してください。


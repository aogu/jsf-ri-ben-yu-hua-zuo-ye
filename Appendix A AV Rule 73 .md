## AVルール73
デフォルトのコンストラクタは、引数なしで呼び出すことができるコンストラクタです。
引数を持たないコンストラクタを呼び出すと、呼び出し元から外部情報を渡さなくてもオブジェクトを作成して初期化することができます。
これはオブジェクトのクラスによっては適切かもしれませんが、外部情報を渡すことなく初期化の合理的な手段がないものもあります。
このクラスのオブジェクトでは、デフォルトのコンストラクタが存在するため、操作を進める前に完全なオブジェクトの初期化を保証するために、メンバ関数に追加のロジックを追加する必要があります。
したがって、デフォルトのコンストラクタを無用に使用しないようにすると、完全に初期化されたオブジェクトに対する操作が複雑で効率的になります。

Partが常にSerialNumberを持たなければならない以下の例を考えてみましょう。
例Aは、特定のシリアル番号で識別される部分の名前を返す単一のメソッドgetPartName()のコードを示しています。
パーツが完全に初期化されているかどうかを判断するには、メンバ関数getPartName()に追加のロジックを追加する必要があります。
対照的に、例Bは不要なデフォルトのコンストラクタを持っていません。
対応する実装はよりクリーンでシンプルで効率的です。

例A：
無償のデフォルトコンストラクタ。

		class Part
		{
		public:
		  Part ()
		  { serial_number =unknown;
		  }                               // Default constructor:

		  Part (int32 n) : serial_number(n) {}
		  
		  int32 get_part_name()
		  {
		    if (serial_number == unknown) // Logic must be added to check for
		    {                             // uninitialized state
		      return "";
		    }
		    else
		    {
		      return lookup_name (serial_number);
		    }
		  }
		private:
		  int32 serialNumber;
		  static const int32 unknown;
		};

例B：
無償のデフォルトコンストラクタはありません。

		class Part
		{
		public:
		  Part (int32 n) : serial_number(n) {}
		  int32 get_part_name () { return lookup_name (serial_number);}
		private: 
		  int32 serial_number;
		}; 

注意：
デフォルトのコンストラクタが存在しない場合は、そのようなオブジェクトの配列とテンプレートベースのコンテナに一定の制限があることを意味します。
詳細は、Meyers [7]を参照してください。


## AVルール204
AVルール204は、不明瞭で、誤解を招くか、あいまいであるか、またはそうでない場合には不特定のまたは未定義の挙動をもたらすであろう発現における副作用を禁止しようとする。
したがって、副作用を伴う操作は、以下のコンテキストでのみ使用されます。

注意：
副作用が定数式と組み合わせて発生することは許されます。
ただし、追加の副作用が式内に隠されないように注意する必要があります。

注意：
関数f（）、g（）、h（）には副作用があります。

1. 単独で
		++i;                           // Good
		for (int32 i=0 ; i<max ; ++i)  // Good: includes the expression portion of a
		                               // for statement
		i++ - ++j;                     // Bad: operation with side-effect doesn’t occur by itself. 

2. 代入の右辺
		y = f(x);                           // Good
		y = ++x;                            // Good: logically the same as y=f(x)
		y = (-b + sqrt(b*b -4*a*c))/(2*a);  // Good: sqrt() does not have side-effect
		y = f(x) + 1;                       // Good: side-effect may occur with a constant
		y = g(x) + h(z);                    // Bad: operation with side-effect doesn’t occur by itself
		                                    // on rhs of assignment
		k = i++ - ++j;                      // Bad: same as above
		y = f(x) + z;                       // Bad: same as above 

3. 条件
		if (x.f(y))        // Good
		if (int x = f(y))  // Good: this form is often employed with dynamic casts
		                   // if (D* pd = dynamic_cast<D*> (pb)) {…}
		if (++p == NULL)   // Good: side-effect may occur with a constant
		if (i++ - --j)     // Bad: operation with side-effect doesn’t occur by itself
		                   // in a condition 

4. 関数呼び出しで副作用を伴う唯一の引数式
		f(g(z));       // Good
		f(g(z),h(w));  // Bad: two argument expressions with side-effects
		f(++i,++j);    // Bad: same as above
		f(g(z), 3);    // Good: side-effect may occur with a constant 

5. ループの状態
		while (f(x))           // Good
		while(--x)             // Good

		while((c=*p++) != -1)  // Bad: operation with side-effect doesn’t occur by itself
		                       // in a loop condition 

6. スイッチ条件
		switch (f(x))      // Good
		
		switch (c = *p++)  // Bad: operation with side-effect doesn’t occur by itself
		                   // in a switch condition 

7. 連鎖操作の単一部分
		x.f().g().h();   // Good
		a + b * c;       // Good: (operator+() and operator*() are overloaded)
		cout << x << y;  // Good 


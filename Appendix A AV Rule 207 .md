## AVルール207
カプセル化されていないグローバルデータは危険なので避けるべきです。
getおよびsetメソッドのみを持つオブジェクト、または各属性のgetおよびsetメソッドはカプセル化されていないとみなされます。

		int32 x=0;  // Bad: Unencapsulated global object.
		
		class Y {
		  in32 x;
		  public:
		    Y(int32 y_);
		    int32 get_x();
		    void set_x();
		};

		Y y (0);  // Bad: Unencapsulated global object. 


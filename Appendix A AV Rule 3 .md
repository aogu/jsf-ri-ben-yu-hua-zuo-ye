## AVルール3
循環的複雑度は、単一のソフトウェアモジュール内の決定ロジックの量を測定します。
それは2つの関連する目的のために使用されるかもしれません：設計の複雑さの尺度とテストの援助。
第1に、ソフトウェアの信頼性、テスト容易性、および保守性を向上させるために、設計から始めて、サイクロマティックな複雑さをソフトウェアライフサイクルの全段階で利用することができます。
第2に、循環的複雑さは、所与のモジュールに必要な試験の数を近似することによって試験計画プロセスを助ける。
循環的複雑度は、コード全体の制御フローに完全に基づいた構造的なメトリックです。
それはコードを通る非反復パスの数です。

循環的複雑度(v(G))は、各モジュールに対して次のように定義されます。

		v(G) = e - n + 2

nは「ノード」または計算文を表し、eは「エッジ」またはノード間の制御の転送を表します。

以下は、対応するノード図が後に続くソースコードの例です。
ノード図では、ステートメントは長方形として、決定は三角形として、そしてステートメント間の線は線として示されています。
ノードの数は14であり、ノードを接続する線の数は17であり、複雑さは5である。

複雑さを推定する別の手段も示されている。
関数の外側の「無限」領域を含む線で囲まれた領域の数は、一般に、計算された複雑さと同等です。
この図には5つの離れた領域があります。
計算された複雑さと等しいことに注意してください。

図は、マルチウェイ決定またはswitch文を使用しています。
多くの場合、switchステートメントでは複雑さが高くなるケースが多いかもしれませんが、コードはまだ理解しやすいです。
したがって、究極の目標である賢明で保守可能なコードを念頭に置いて、複雑さの制限を設定する必要があります。

例：

            void compute_pay_check ( employee_ptr_type employee_ptr_IP, check_ptr_type chk_ptr_OP )
            {
              //Calculate the employee’s federal, fica and state tax withholdings
        1.    chk_ptr_OP->gross_pay = employee_ptr_IP->base_pay;
        2.    chk_ptr_OP->ged_tax = federal_tax ( employee_ptr_IP->base_pay );
        3.    chk_ptr_OP->fica = fica ( employee_ptr_IP->base_pay );
        4.    chk_ptr_OP->state_tax = state_tax ( employee_ptr_IP->base_pay );
              //Determine medical expense based on the employee’s HMO selection
        5.    if ( employee_ptr_IP->participate_HMO == true ) 
              {
        6.      chk_ptr_OP->medical = med_expense_HMO;
              }
              else
              {
        7.      chk_ptr_OP->medical = med_expense_non_HMO;
              }
              // Calc a profit share deduction based on % of employee’s gross pay
        8.    if (employee_ptr_IP->participate_profit_share == true )
              {
        9.      switch( employee_ptr_IP->profit_share_plan )
                {
                case plan_a:
        10.       chk_ptr_OP->profit_share = two_percent * chk_ptr_OP->gross_pay;
                  break;
                case plan_b:
        11.       chk_ptr_OP->profit_share = four_percent * chk_ptr_OP->gross_pay;
                  break;
                case plan_c:
        12.       chk_ptr_OP->profit_share = six_percent * chk_ptr_OP->gross_pay;
                  break;
                default:
                  break;
                }
              }
              else
              {
        13.     chk_ptr_OP->profit_share = zero;
              }
              chk_ptr_OP->net_pay = ( chk_ptr_OP->gross_pay –
                chk_ptr_OP->fed_tax –
                chk_ptr_OP->fica –
                chk_ptr_OP->state_tax –
                chk_ptr_OP->medical –
                chk_ptr_OP->profit_share );
              }

例：

訳注：
オリジナル文書68ページの図を参照。


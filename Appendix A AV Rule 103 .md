## AVルール103
Stroustrup [4]は、最小限の労力を要し、追加のコードを生成する必要がなく、コンパイラが許容可能なエラーメッセージ（単語制約を含む）を生成するようにする（テンプレートパラメータ制約を作成する）ソリューションを提供する。

さらに、Stroustrupは、派生、代入、比較および乗算に関与するテンプレートパラメータの能力をチェックする次のサンプル制約を提供します。 
（以下の要素は、制約ライブラリの適切な候補であることに注意してください）。

		template<class T, class B> struct Derived_from {
		  static void constraints(T* p) { B* pb = p; }
		  Derived_from() { void(*p)(T*) = constraints; }
		};
	
		template<class T1, class T2> struct Can_copy {
		  static void constraints(T1 a, T2 b) { T2 c = a; b = a; }
		  Can_copy() { void(*p)(T1,T2) = constraints; }
		};

		template<class T1, class T2 = T1> struct Can_compare {
		  static void constraints(T1 a, T2 b) { a==b; a!=b; a<b; }
		  Can_compare() { void(*p)(T1,T2) = constraints; }
		};

		template<class T1, class T2, class T3 = T1> struct Can_multiply {
		  static void constraints(T1 a, T2 b, T3 c) { c = a*b; }
		  Can_multiply() { void(*p)(T1,T2,T3) = constraints; }
		}; 

したがって、上記のCan_copy制約を前提として、ShapeへのポインタまたはShape（またはShapeへの変換可能）からパブリックに派生したクラスへのポインタで構成されるコンテナのみが渡されることをコンパイル時にアサートするdraw_all（）関数を記述することができます 。

		template<class Container>
		void draw_all(Container& c)
		{
		  typedef typename Container::value_type T;
		  Can_copy<T,Shape*>(); // accept containers of only Shape*’s
		  for_each(c.begin(),c.end(),mem_fun(&Shape::draw));
		} 

追加の制約を簡単に作成できます。
制約の作成と使用に関する詳細については、[4]を参照してください。


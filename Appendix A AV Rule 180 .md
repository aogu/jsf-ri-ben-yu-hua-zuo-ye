## AVルール180
次の例は、情報が失われる暗黙的な変換を示しています。

		int32    i =1024;
		char     c = i;          // Bad: (integer-to-char) implicit loss of information.
		float32  f = 7.3;
		int32    j= f;           // Bad: (float-to-int) implicit loss of information.
		int32    k = 1234567890;
		float32  g = k;          // Bad: (int-to-float) implicit loss of information
		                         // (g will be 1234567936) 

狭いタイプへの明示的なキャスト（情報の損失が発生する可能性がある）は、特にアルゴリズム的に必要な場合にのみ使用することができます。
明示的なキャストは、情報の損失が可能であり、適切な緩和が適切に行われなければならないという事実に注意を引く。


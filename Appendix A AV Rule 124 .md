## AVルール124
シンプルな転送機能は、以下に示すようにインライン化する必要があります。

例A：

		inline draw() // Example of a forwarding function that should be inlined
		{
		  draw_foreground ();
		} 


## AVルール192
最後のelse句（または、なぜ最後のelse句が不要であるかを示すコメント）を提供すると、次の例に示すように、else ifシリーズですべてのケースが処理されます。

例A：
if elseがないので、最後のelse節は不要です。

		if (a < b)
		{
		  foo();
		}

例B：
前の条件のいずれも満たされない場合に必要な最後のelse節。

		if (a < b)
		{
		  ...
		}
		else if (b < c)
		{
		  ...
		}
		else if (c < d)
		{
		}
		else  // Final else clause needed
		{
		  handle_error();
		} 

例C：
可能なすべての条件が処理されるため、最後のelse節は不要です。
したがって、この状態を明確にするためのコメントが含まれています。

		if (status == error1)
		{
		  handle_error1();
		}
		else if (status == error2)
		{
		  handle_error2()
		}
		else if (status == error3)
		{
		  handle_error3()
		} // No final else needed: all possible errors are accounted for. 


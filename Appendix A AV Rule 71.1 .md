## AVルール71.1
AVルール71.1の目的は、クラスの仮想関数がそのコンストラクタまたはそのデストラクタのいずれかで静的に解決されることを明確にすることです。
結果として、コンストラクタ/デストラクタで仮想関数を配置すると、しばしば予期しない動作が発生します。

以下の例を考えてみましょう。
例Aでは、仮想関数は多態的な振る舞いを示さない。
これとは対照的に、例Bでは同じ関数が呼び出されていますが、今回はスコープ解決演算子を使用して、仮想関数が静的にバインドされていることを明確にしています。

例A：

		class Base
		{
		public:
		  Base()
		  {
		    v_fun(); // Bad: virtual function called from constructor. Polymorphic
		  }          // behavior will not be realized.
		  virtual void v_fun()
		  {
		  }
		}; 

例B：

		class Base
		{
		public:
		  Base()
		  {
		    Base::v_fun(); // Good: scope resolution operator used to specify static
		  }                // binding
		  virtual void v_fun()
		  {
		  }
		};


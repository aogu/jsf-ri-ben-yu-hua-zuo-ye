## AVルール77
クラスには、継承階層内に存在するだけでなく、多くのデータメンバーが含まれる場合があります。
したがって、コピーコンストラクタは、次の例のように、基本クラスのメンバーを含むすべてのメンバー（クラス不変式に影響する）をコピーする必要があります。

		class Base
		{
		  public:
		    Base (int32 x) : base_member (x) { }
		    Base (const Base& rhs) : base_member (rhs.base_member) {}
		  private:
		    int32 base_member;
		};

		class Derived : public Base
		{
		  public:
		    Derived (int32 x, int32 y, int32 z) : Base (x),
		                                          derived_member_1 (y),
		                                          derived_member_2 (z) { }
		
		    Derived(const Derived& rhs) : Base(rhs),
		                                  derived_member_1 (rhs.derived_member_1),
		                                  derived_member_2 (rhs.derived_member_2) { }

		  private:
		    int32 derived_member_1;
		    int32 derived_member_2;
		};


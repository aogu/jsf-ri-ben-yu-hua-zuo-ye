use File::Basename qw/basename/;

my $Number_of_blank_rows_required = 2;

my @files = glob "../*.md";
@files = sort { alphanum($a, $b) } @files;

foreach my $file (@files) {
	if (basename($file) =~ /^0/)
	{
		next;
	}
	
	open(my $fh, "+<", $file);

	my $emp_cnt = 0;

	my $last_line_exists_NLchar = 0;

	while (my $line = readline $fh)
	{
		$last_line_exists_NLchar = $line =~ /\n/ ? 1 : 0;

		$line =~ s/\r\n/\n/;
		chomp $line;

		if ($line =~ m/^\s*$/)
		{
			$emp_cnt++;
		}
		else
		{
			$emp_cnt = 0;
		}
	}

	if ($last_line_exists_NLchar)
	{
		$emp_cnt++;
	}

	while ($emp_cnt++ < $Number_of_blank_rows_required)
	{
		print $fh "\n";
	}

	close($fh);

	print "$emp_cnt $file\n";
}

sub alphanum {
  # split strings into chunks
  my @a = chunkify($_[0]);
  my @b = chunkify($_[1]);

  # while we have chunks to compare.
  while (@a && @b) {
    my $a_chunk = shift @a;
    my $b_chunk = shift @b;

    my $test =
        (($a_chunk =~ /\d/) && ($b_chunk =~ /\d/)) ? # if both are numeric
            $a_chunk <=> $b_chunk : # compare as numbers
            $a_chunk cmp $b_chunk ; # else compare as strings

    # return comparison if not equal.
    return $test if $test != 0;
  }

  # return longer string.
  return @a <=> @b;
}

# split on numeric/non-numeric transitions
sub chunkify {
  my @chunks = split m{ # split on
    (?= # zero width
      (?<=\D)\d | # digit preceded by a non-digit OR
      (?<=\d)\D # non-digit preceded by a digit
    )
  }x, $_[0];
  return @chunks;
}

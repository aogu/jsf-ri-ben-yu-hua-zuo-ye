use File::Basename qw/basename/;
use List::Util qw/max min/;

my @files = glob "../*.md";
@files = sort { alphanum($a, $b) } @files;

open(my $outfh, ">", "../0 Index .md");

print $outfh "\n";

foreach my $file (@files) {
	if (basename($file) =~ /^0/)
	{
		next;
	}
	
	open(my $fh, "<", $file);

	my @rules;

	while (my $line = readline $fh)
	{
		chomp $line;

		if ($line !~ /## AV/)
		{
			next;
		}

		if ($line =~ /## AV(ルール|規則)([0-9]+(.[0-9])?)/)
		{
			push(@rules, $2);
		}
	}

	close($fh);

	my $filename = basename($file, ".md");
	$filename =~ s/^ *(.*?) *$/$1/;

	$filename =~ /(([0-9]+.)+)/;
	my $periodCount = (($hoge) = $1 =~ m/\./g);

	my $titleHdr;

	if (basename($file) =~ /^Appendix/)
	{
		$titleHdr = "###";
	}
	else
	{
		$titleHdr = substr(sprintf("%0*d", $periodCount + 1, 0), 1);
		$titleHdr =~ s/0/#/g;
		$titleHdr .= "##";
	}

	my $title = sprintf("$titleHdr [$filename](%s)", basename($file));

	print $outfh $title, "\n";
	print $outfh "\n";


	if ($file =~ /Appendix/)
	{
		next;
	}


	my $count = @rules;

	if ($count == 0)
	{
		next;
	}

	my $ruleStr = 
		$count == 1
			? "ルール@rules[0]"
			: "ルール@rules[0]-@rules[$count - 1]";

	print $outfh $ruleStr, "\n";
	print $outfh "\n";
}

close($outfh);


sub alphanum {
  # split strings into chunks
  my @a = chunkify($_[0]);
  my @b = chunkify($_[1]);

  # while we have chunks to compare.
  while (@a && @b) {
    my $a_chunk = shift @a;
    my $b_chunk = shift @b;

    my $test =
        (($a_chunk =~ /\d/) && ($b_chunk =~ /\d/)) ? # if both are numeric
            $a_chunk <=> $b_chunk : # compare as numbers
            $a_chunk cmp $b_chunk ; # else compare as strings

    # return comparison if not equal.
    return $test if $test != 0;
  }

  # return longer string.
  return @a <=> @b;
}

# split on numeric/non-numeric transitions
sub chunkify {
  my @chunks = split m{ # split on
    (?= # zero width
      (?<=\D)\d | # digit preceded by a non-digit OR
      (?<=\d)\D # non-digit preceded by a digit
    )
  }x, $_[0];
  return @chunks;
}

## AVルール70.1
概念的には、開発者は、オブジェクトが作成される前、またはオブジェクトが破棄される前に、オブジェクトを使用すべきではないことを理解しています。
しかしながら、この区別が明白でない場合がある多数のシナリオが生じるかもしれない。
したがって、混乱の可能性のある領域を強調する一連の例が提供されています。
多くの場合、C++標準[10]が引用され、説明コードセグメントが提供されています。

例A：
main()を終了します。

main()は、それが含まれているアプリケーションとは独立して決して終了してはいけません。
以下のコードサンプルを検討してください。
main()が終了すると、静的オブジェクトデストラクタが呼び出されます。
したがって、main()によって作成されたタスクは、それらの静的オブジェクトの存在に依存することはできません。

		int32 main()
		{
		  _main();             // Call static constructors (inserted by compiler)
		                       // Application code begins
		  initialize_task_1(); // Initialize tasks
		  initialize_task_2();
		...
		  initialize_task_n();
		                       // Application code ends
		  __call_dtors();      // Call static destructors (inserted by compiler)
		}
		                       // Tasks begin to run. However, static objects have been destroyed. 

例B：
構築中のconstオブジェクトへのアクセス

このシナリオは、AVルール207によって禁止されているグローバル変数を使用しないと発生しないことに注意してください。

constオブジェクトの構築中に、コンストラクタのthisポインタから直接的または間接的に取得されない左辺値を介してオブジェクトまたはそのサブオブジェクトの値にアクセスすると、そのようにして取得されたオブジェクトまたはサブオブジェクトの値は指定されません 。 [10] 12.1（15）

		struct C;
		void no_opt(C*);
		struct C
		{
		  int c;
		  C() : c(0)
		  {
		    no_opt(this);
		  }
		};

		const C cobj;
		void no_opt(C* cptr)
		{
		  int i = cobj.c * 100 // value of cobj.c is unspecified
		  cptr->c = 1;
		  cout << cobj.c * 100 // value of cobj.c is unspecified
		  << ’\n’;
		} 

例C：
非自明なデストラクタを持つローカル静的オブジェクト

関数が静的記憶期間のローカルオブジェクトを含み、静的記憶期間を有するオブジェクトの破壊の間に関数が呼び出される場合、制御フローが以前に破壊されたローカルの定義を通過する場合、プログラムは未定義の挙動を有する オブジェクト。 [10] 3.6.3（2）

		class A
		{
		public:
		  ~A() { … }
		};

		void foo()
		{
		  static A a;   // Destructor of local static will be invoked on exit
		}

		class B
		{
		public:
		  ~B()
		  {
		    foo();      // Destructor of static calls function with local static which may
		  }             // already be destroyed.
		};

		static B B_var; // Destructor of static will be invoked on exit. 

例D：
オブジェクトの存続期間が終了した後のメンバ関数の呼び出し

オブジェクトの存続期間が開始される前に、オブジェクトが占有する記憶装置が割り当てられた後、またはオブジェクトの存続期間が終了した後、およびオブジェクトが占有した記憶装置が再使用または解放される前に、 対象物が置かれた場所または使用された場所は、限定された方法でしか使用されない。
...オブジェクトがPOD以外のクラス型であった場合、そのプログラムは未定義の動作をします。

* ポインタは、非静的なデータメンバにアクセスするため、またはオブジェクトの非静的メンバ関数を呼び出すために使用されます... [10] 3.8（5）

		struct B
		{
		  virtual void f();
		  void mutate();
		  virtual ~B();
		};

		struct D1 : B
		{
		  void f()
		};

		struct D2 : B
		{
		  void f()
		};

		void B::mutate()
		{
		  new (this) D2; // reuses storage – ends the lifetime of *this
		  f();           // undefined behavior
		  ... = this;    // OK, this points to valid memory
		}
		                 // Note: placement new is only allowed in low-level memory
		                 // management routines (see AV Rule 206). 

例E：
ストレージの再利用に暗黙的なデストラクタ呼び出しは必要ありません。

重大ではないデストラクタを持つクラス型のオブジェクトの場合、プログラムは、オブジェクトが占有する記憶域が再利用または解放される前にデストラクタを明示的に呼び出す必要はありません。
ただし、デストラクタへの明示的な呼び出しがない場合、または削除式（5.3.5）がストレージの解放に使用されない場合、デストラクタは暗黙的に呼び出されてはならず、デストラクタによって生成される副作用に依存するプログラム 定義されていない動作をします。 [8] 3.8（4）

		struct A
		{
		  ~A()
		  {
		    …non-trivial destructor
		  }
		};
		
		struct B { … };
		
		void c_03_06_driver()
		{
		  A a_obj;
		  new (&a_obj) B(); // a_obj’s lifetime ended without calling
		  …                 // nontrivial destructor.
		}
		                    // Note: placement new is only allowed in low-level memory
		                    // management routines (see AV Rule 206). 

例F：
暗黙のデストラクタ呼び出しのために元の型のオブジェクトを格納する必要があります。

プログラムが静的（3.7.1）または自動（3.7.2）の記憶期間でタイプTのオブジェクトの存続期間を終了し、Tに自明でないデストラクタがある場合、プログラムは元のタイプのオブジェクトが占有していることを保証する必要があります 暗黙的なデストラクタ呼び出しが行われるときと同じ格納場所。
それ以外の場合、プログラムの動作は未定義です。
たとえブロックが例外を出して終了したとしても、これは当てはまります。 3.8（8）

		class T { };

		struct B {
		  ~B() { … };
		};

		void c_03_11_driver()
		{
		  B b;
		  new (&b) T; // B’s nontrivial dtor implicitly called on memory occupied by an
		              // object of different type.
		}             //undefined behavior at block exit
		              // Note: placement new is only allowed in low-level memory
		              // management routines (see AV Rule 206). 

例G：
constオブジェクトの格納場所に新しいオブジェクトを作成する

静的または自動記憶期間を有するconstオブジェクトが占有する記憶場所に新しいオブジェクトを作成するか、またはそのようなconstオブジェクトがその存続期間が終了する前に占有していた記憶場所において、定義されていない動作が生じる。 3.8（9）

		struct B
		{
		  B() { … };
		  ~B() { … };
		};
		const B b;
		void c_03_12_driver()
		{
		  b.~B();           // A new object is created at the storage location that a const
		                    // object used to occupy before its lifetime ended. This results
		  new (&b) const B; // in undefined behavior
		}
		                    // Note: placement new is only allowed in low-level memory
		                    // management routines (see AV Rule 206). 

例H：
ベースが初期化される前に呼び出されたctor-Initializerのメンバー関数

これらの操作（メンバ関数呼び出し、typeidまたはdynamic_castのオペランド）がctor-initializer（またはctorinitializerから直接的または間接的に呼び出された関数）で実行され、基本クラスのすべてのmem-initializerが完了すると、 操作は未定義です。 12.6.2（8）

		class A { public: A(int) { … }};
		class B : public A
		{
		  int j;
		  public:
		  int f() { … };
		  B() : A(f()), // Undefined: calls member function but base A is
		                // is not yet initialized
		  j(f()) { … }  // Well-defined: bases are all initialized
		};


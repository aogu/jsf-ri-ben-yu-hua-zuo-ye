## AVルール185
伝統的なCスタイルのキャストはいくつかの懸念を提起する。
まず、変換の理由を示すことなくほとんどの型を他のほとんどの型に変換できます。
次に、Cスタイルのキャスト構文を次に示します。

		(type) expression // convert expression to be of type type. 

レビューアとツールの両方を識別することは困難です。
結果として、変換式の位置付けとその後の変換テナントの分析の両方が、Cスタイルのキャストでは困難であることが判明する。

したがって、C++ではいくつかの新しいスタイルのキャスト（const_cast、dynamic_cast(4)、reinterpret_cast、およびstatic_cast）が含まれています。
新しいスタイルのキャストは、次の形式をとります。

		const_cast<type>        (expression)  // convert expression to be of type type.
		reinterpret_cast<type>  (expression)
		static_cast<type>       (expression)

これらのキャストは識別しやすいだけでなく、キャストを適用する開発者の意図をより正確に伝達します。

派生クラスと基底クラス間の変換に関するルールAVルール178も参照してください。

(4)
ダイナミックキャストはツールのサポートがないためこの時点では使用できませんが、SEAL1 / 2ソフトウェアの適切な調査が行われた後、将来的に検討されることに注意してください。


## AVルール95
C++は仮想メソッドを動的にバインドしますが、これらのメソッドのデフォルトパラメータは静的にバインドされています。
したがって、基本型ポインタ（Shape *）で参照される場合、派生型（Circle）のdraw（）メソッドは、基本型（Shape）のデフォルトパラメータで呼び出されます。

例A：

		enum Shape_color { red, green, blue };
		class Shape
		{
		  public:
		    virtual void draw (Shape_color color = green) const;
		  …
		}
		class Circle : public Shape
		{
		  public:
		    virtual void draw (Shape_color color = red) const;
		  …
		}
		void fun()
		{
		  Shape* sp;
		  sp = new Circle;
		  sp->draw (); // Invokes Circle::draw(green) even though the default
		}              // parameter for Circle is red. 


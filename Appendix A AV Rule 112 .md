## AVルール112
次の例は、関数の戻り値がリソースの所有を隠し、リソースのリークを危険にさらす可能性のあるいくつかの方法を示しています。
次の例では、ヒープからメモリを割り当てる必要はありませんが、問題のクラスでオーバーロードされる可能性があります。

例A：
呼び出し元がオブジェクトを削除することを覚えておく必要があるため、newで初期化された逆参照ポインタを返すとエラーが発生しやすくなります。
そのオブジェクトが一時的なオブジェクトである場合、これはより困難になります。

		X& f (float32 a)
		{
		  return *new x(a);         // Error prone. Caller must remember to perform
		}                           // the delete.
		
		X& ref = f(1);              // The caller of f() must be responsible for deleting
		…                           // the memory.
		delete &ref                 // delete must be called for every invocation of f().
		…
		X& x = f(1)*f(2)*f(3)*f(4); // Memory leak: delete not called for temporaries. 

例B：
ローカルオブジェクトへのポインタを返すことは、オブジェクトがリターン後に存在しなくなるため問題になります。
AVルール111はこの慣行を明示的に禁じている。

		X* f (float32 a) // Error: the caller most likely believes he is
		{                // responsible for deleting the object. However, the object
		  X b(a);        // ceases to exist when the function returns.
		  return &b
		} 

例C：
関数はオブジェクトへのポインタを返すことができますが、受信者は削除を実行することを忘れないでください。

		X *f(float32 a)
		{
		  return new X(a); // Beware of leak: recipient must remember to perform the delete.
		} 

例D：
オブジェクトを値で返すことは、所有権の問題を隠すことのない簡単な方法です。

		X f(float 32 a) // Simple and clear.
		{
		  X b(a);
		  return b;
		} 


## AVルール29
インライン関数は、テキストの置換を必要とせず、引数を指定して呼び出すと正常に動作します（たとえば、型チェックが実行されます）。

例：2つの整数の最大値を計算します。

		#define max (a,b) ((a > b) ? a : b)  // Wrong: macro
		
		inline int32 maxf (int32 a, int32 b) // Correct: inline function
		{
		  return (a > b) ? a : b;
		}

		y = max (++p,q);                     // Wrong: ++p evaluated twice
		y=maxf (++p,q)                       // Correct: ++p evaluated once and type
		                                     // checking performed. (q is const) 

訳注：
`#define max (a,b) ((a > b) ? a : b)`というマクロが有った場合、`y = max(++p, q)`というコードは、`y = ((++p > q) ? ++p : q)`と展開されます。
この場合、pが2回インクリメントされる危険があります。
また、マクロでは型チェックが行われないため、設計時には考慮されていなかった型の判定が行われる危険性があります。


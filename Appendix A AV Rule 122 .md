## AVルール122
次の例は、簡単なアクセサと簡単なミューテータをインライン化するクラスを示しています。

		class Example_class
		{
		  public:
		    int32 get_limit (void)            // Sample accessor to be inlined
		    {
		      return limit;
		    }
		    void set_limit (int32 limit_parm) // Sample mutator to be inlined
		    {
		      limit = limit_parm;
		    }
		  private:
		    int32 limit;
		}; 


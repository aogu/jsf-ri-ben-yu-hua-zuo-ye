## AVルール93
例Aは、メンバーName、Address、およびPhoneNumberで構成されたPersonクラスを示しています。
したがって、Personの機能はメンバー要素（Name、Address、およびPhoneNumber）の観点から実装されます。

例A：

		class Person
		{
		  private:
		    string name; // Person is composed of members Name, Address, and
		                 // PhoneNumber
		    string address;
		    string phone_number;
		...
		}; 

一般的には、保護されたメンバーや仮想メソッドへのアクセスが必要な場合を除き、メンバーシップを使用する必要があります。
このような状況では、メンバーシップは機能しません。
代わりに、非公開の継承を使用する必要があります。
Meyers [6]、アイテム43によって提供されるGenericStackの例を考えてみましょう。
例Bに示すように、GenericStack実装を任意のタイプのスタックに再利用することができます。
しかし、GenericStackの実装は、 。
代わりに、型セーフなインターフェイスはテンプレートクラスを通じて提供されます。
GenericStackのメソッドはprotectedと宣言され、このクラスを型セーフなインターフェイスから分離して使用しないようにします。
その結果、派生クラスは、クラスメンバシップではなく、継承を介してGenericStackの保護されたメンバを使用する必要があります。

例B：

		class Generic_stack
		{
		  protected: // Methods are protected so that Generic_stack
		             // cannot be used by itself.
		    Generic_stack();
		    ~Generic_stack();
		    void  push (void *object);
		    void* pop (void);
		    bool  empty () const;
		  private:
		  ...
		}; 

GenericStackのタイプセーフなインターフェイスは、次のように実装できます。

		template<class T>
		class Stack: private Generic_stack // Reuse base class implementation
		{
		  public:
		    void push (T *object_ptr) { GenericStack::push (object_ptr); }
		    T * pop (void) { return static_cast<T*>(Generic_stack::pop()); }
		    bool empty () const { return Generic_stack::empty(); }
		}; 


## AVルール210.1
この規則は、アプリケーションが、アクセス指定子で区切られた非静的データメンバーの順序付けに関する順序を仮定することを禁止することを目的としています。

以下の例Aを検討してください。
クラスAは着信メッセージデータに確実に「オーバーレイ」することはできません。
属性の順序付け（アクセス指定子を問わず）が指定されていないためです。

対照的に、構造Bは同じ着信メッセージデータに確実に「オーバーレイされる」ことがあります。

例A：

		class A
		{
		  ...
		  protected:  // a could be stored before b, or vice versa
		    int32 a;
		  private:
		    int32 b;
		};
		...
		              // Bad: application assumes that objects of
		              // type A will always have attribute a
		              // stored before attribute b.
		A* a_ptr = static_cast<A*>(message_buffer_ptr);

例B：

		struct B
		{
		  int32 a;
		  int32 b;
		};
		...
		// Good: attributes in B not separated
		// by an access specifier
		B* b_ptr = static_cast<B*>(message_buffer_ptr);


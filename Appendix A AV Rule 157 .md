## AVルール157
短絡オペレータを使用する場合は注意が必要です。
たとえば、次のコードの論理式がfalseと評価された場合、変数xはインクリメントされません。
これは、後続のステートメントでxがインクリメントされていると仮定することがあるため、問題になる可能性があります。

		if ( logical_expression && ++x) // Bad: right-hand side not evaluated if the logical
		                                //      expression is false.
		...
		f(x);                           // Error: Assumes x is always incremented.
		...


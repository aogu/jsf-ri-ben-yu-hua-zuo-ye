## AVルール39
ヘッダーファイルには、一般的に非const変数や関数定義を含めるべきではありませんが、インライン関数やテンプレート定義が含まれることがよくあります。

例：
定義は一般的には.cppファイルに配置する必要がありますが、クラス宣言内に定義されているメンバ関数は、（可能であれば）メンバ関数をインライン化する必要があることをコンパイラに示すものです。

		class Square
		{
		public:
		  float32 area()          // The member function definition in the class declaration
		  {                       // suggests to the compiler that the member function should be
		    return length *width; // inlined.
		  }
		private:
		  float32 length;
		  float32 width;
		};


## AVルール204.1
演算子と部分式が評価される順序は指定されていないので、式は、標準が許す任意の順序で同じ値を生成するように記述する必要があります。

		i = v[i++];         // Bad: unspecified behavior
		i = ++i + 1;        // Bad: unspecified behavior
		p->mem_func(*p++);  // Bad: unspecified behavior 


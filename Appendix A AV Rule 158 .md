## AVルール158
このルールの目的は、括弧を必要とすることです。
ただし、括弧で囲まれた式の不足を止めながら、明瞭さを向上させることができます。
次の例では、論理演算子&&または||のオペランド（2項演算子を含む）をカッコで括弧でくくっています。
読みやすさを向上させます。

例：

		valid (p)   &&  add(p)    // parenthesis not required
		x.flag      &&  y.flag     // parenthesis not required
		a[i]        ||  b[j]       // parenthesis not required
		
		(x < max )  &&  (x > min)  // parenthesis required
		(a || b)    &&  (c || d)   // parenthesis required 


## AVルール42
AVルール42は、expression文が別々の行になければならないことを示します。 式ステートメントの形式は次のとおりです。

		expression-statement:
		  expressionopt ;

式ステートメントのすべての式が評価され、次のステートメントが実行される前にすべての副作用が完了します。
最も一般的な式文は、代入と関数呼び出しです。 [10]

例:

		x = 7; y=3;            // Incorrect: multiple expression statements on the same line.
		a[i] = j[k]; i++; j++; // Incorrect: multiple expression statements on the same line.
		a[i] = k[j];           // Correct.
		i++;
		j++; 

注意：
forステートメントは、条件optと式optがexpression-statement [10]と同じ行に表示される特殊なケースです。

		iteration-statement:
		  while ( condition ) statement
		  do statement while ( expression ) ;
		  for ( for-init-statement ; condition-opt ; expression-opt ) statement

		for-init-statement:
		  expression-statement
		  simple-declaration 

例：

		for( i = 0 ; i < max ; ++i) fun(); // Incorrect: multiple expression statements on the same line.
		for(i = 0 ; i < max ; ++i)         // Correct
		{
		  foo();
		}


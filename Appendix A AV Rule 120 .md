## AVルール120
関数のオーバーロードは、引数型に関してのみ異なる関連演算のファミリを作成するための強力なツールになります。
しかし、一貫して使用されないと、過負荷はかなりの混乱を招く可能性があります。

例A：
関数のオーバーロードの適切な使用法を以下に示します。
contains（）のすべてのオーバーロードは、同じ概念タスクを実行するだけでなく、同じ名前を共有します。

		class String
		{
		  public:                               // Used like this:
		    // ...                              // String x = "abc123";
		    int32 contains ( const char c );    // int32 i = x.contains( 'b' );
		    int32 contains ( const char* cs );  // int32 j = x.contains( "bc1" );
		    int32 contains ( const String& s ); // int32 k = x.contains( x );
		    // ...
		}; 

例B：
オペレータ過負荷の不適切な使用を以下に示します。
2次元ベクトルの場合、演算子*（）はドット積を意味し、3次元ベクトルの場合、演算子*（）はクロス積を意味する。

		Vector2d {
		  public:
		    float32 operator*(const Vector2d & v); // compute dot product
		    ...
		};

		Vector3d {
		  public:
		    Vector3d operator*(const Vector3d & v) // compute cross product
		    ...
		}; 


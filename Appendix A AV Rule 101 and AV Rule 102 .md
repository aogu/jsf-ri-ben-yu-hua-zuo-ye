## AVルール101およびAVルール102
多くのテンプレートのインスタンス化が生成されるため、レビューとテストの目的で実際のインスタンス化のリストを提供するようにコンパイラを構成する必要があります。
次の表は、float32とint32の両方の型に対してインスタンス化されたStackクラスの出力を示しています。
完全なテスト計画が構築されるように、メソッドのインスタンス化がリストされていることに注意してください。

| Template                      | Parameter Type    | Library/Module                  |
|:------------------------------|:------------------|:--------------------------------|
|Stack<T1>::Stack<float32>(int) | [with T1=float32] | shape_hierarchy.a(shape_main.o) |
|Stack<T1>::Stack<int32>(int)   | [with T1=int32]   | shape_hierarchy.a(shape_main.o) |
|T1 Stack<T1>::pop()            | [with T1=float32] | shape_hierarchy.a(shape_main.o) |
|T1 Stack<T1>::pop()            | [with T1=int32]   | shape_hierarchy.a(shape_main.o) |
|void Stack<T1>::push(T1)       | [with T1=float32] | shape_hierarchy.a(shape_main.o) |
|void Stack<T1>::push(T1)       | [with T1=int32]   | shape_hierarchy.a(shape_main.o) |


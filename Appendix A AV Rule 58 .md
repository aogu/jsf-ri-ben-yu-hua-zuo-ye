## AVルール58
例：
次の例は、複数の引数を持つ関数を宣言する正しい方法を示しています。

		int32 max (int32 a, int32 b)        // Correct: two parameters may appear on the
		{                                   // same line. Order is easily understood.
		  ...
		}
		                                    // Incorrect: too many parameters on the same line.
		                                    // Difficult to document parameters in this form
		msg1_in (uint16 msg_ID, float32 rate_IO, uint32 msg_size, uint16 rcv_max_instances)
		{
		  ...
		}
		                                    // Correct form.
		msg1_in ( uint16 msg_ID,            // Unique identifier that is the label for the message
		          float32 rate_IO,          // The desired rate for the message distributed
		          uint32 msg_size,          // Size in bytes of the message
		          uint16 rcv_max_instances) // The maximum number of instances of this
		                                    // message expected in a processing frame
		{
		  ...
		} 


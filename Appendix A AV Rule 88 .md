## AVルール88
このルールのコンテキストでは、次のプロパティを持つクラスによってインターフェイスが指定されます。

* それはインタフェースであることを意図しており、
* パブリックメソッドは純粋な仮想関数であり、
* データ項目が小さく、インターフェースの一部として機能しない限り（例えば一意のオブジェクト識別子）、データを保持しません。

注1：
保護されたメンバは、そのクラスがクライアントインターフェイスに参加していない限り、クラス内で使用できます。

注2：
付加的な状態情報を有するクラスは、テンプレート（例えば、ポリシーベースの設計）を用いて統制された方法で構成が実行されることを条件として、ベースとして使用することができる。
下記の「ポリシーによるプログラミング」の段落を参照してください。

次の図は、多重継承の良い例と悪い例の両方を示しています。

訳注：
オリジナル文書98,99ページの図を参照。

仮想基本クラス：
次の図は、仮想基本クラスと非仮想基本クラスの違いを示しています。
次の図は、仮想基本クラスの正当な使用法を示しています。

訳注：
オリジナル文書100,101ページの図を参照。

クラスインタフェース内の保護されたデータ：
前述のように、保護されたデータは、そのクラスがクライアントインタフェースに参加していない限り、クラス内で使用できます。
次の図は、この点を示しています。

訳注：
オリジナル文書102ページの図を参照。

### ポリシーベースの設計
前述したように、付加的な状態情報を有するクラスは、テンプレート（例えば、ポリシーベースの設計）を用いて統制された方法で構成が実行されることを条件として、ベースとして使用することができる。
クラスがカスタマイズ可能でなければならないが、効率性が最も重要な場合に使用されるプログラミングの一形態をポリシープログラミングといいます。
クラスの機能を独立した懸念のセットに分けることができ、それぞれの関心事を複数の方法でプログラミングできる場合、ポリシープログラミングは非常に便利です。
特に、コードの複製を避けることでメンテナンスを簡素化します。
古典的な例は、行列演算パッケージです。
懸念事項は次のとおりです。
* アクセス - 
要素はどのようにメモリに配置されていますか？
行のメジャー、列のメジャー、および上の三角形が考えられます。
* 割り当て - 
どこからメモリが来るのか？
システムヒープ、固定領域、またはユーザー指定の割り当て方式によって割り当てられる可能性があります。
* エラー処理 - 
エラーが発生したときに実行される処理は何ですか？
いくつかの可能性は、例外をスローする、エラーメッセージを記録する、エラーコードを設定する、またはプロセスを再起動することです。

これらの懸念事項は互いに独立しており、別々にコード化することができます。
例えば：

		template< class T >
		class Row_major
		{
		  public:
		    typedef T value_type;
		    Row_major( int32 nrows, int32 ncols, T* array ) :
		    nrows_(nrows), ncols_(ncols), array_(array)
		                                                   {}
		    ~Row_major()                                   {}
		    int32 size1() const                            { return nrows_; }
		    int32 size2() const                            { return ncols_; }
		    const T& operator() ( int32 i, int32 j ) const { return array_[i*ncols_+j]; }
		    T& operator() ( int32 i, int32 j )             { return array_[i*ncols_+j]; }
		
		private:
		    int32 nrows_;
		    int32 ncols_;
		    T* array_;
		};

括弧演算子がarray_[j*nrows_+i]を返す以外は、Column_majorクラスは非常に似ています。

考えられる各組み合わせのコードを作成するのではなく、それぞれの関心事について実装をまとめたテンプレートクラスを作成します。
したがって、

* アクセスはかっこ演算子を定義します。
* Allocはテンプレートメソッドを定義します。
T * allocate <T>（int32 n）、および
* Errは以下のメソッドを定義します

		void handle_error( int32 code, int32 nr, int32 nc )
		void handle_error( int32 code, int32 i, int32 j, int32 nr, int32 nc ) 

次のようにMatrixクラスを作成できます。

		template< class Access, class Alloc, class Err >
		class Matrix : public Access, Alloc, Err // Alloc and Err are private bases
		{
		Matrix( int32 nrows, int32 ncols ) :
		    Access(nrows,ncols,allocate<T>(nrows*ncols))
		{
		  if( array_==0 )
		  {
		    handle_error( Err::allocation_failed, nrows, ncols );
		  }
		}

		Access::value_type& at( int32 i, int32 j )
		{
		  if( i<0 || i>nrows_ || j<0 || j>ncols_ )
		  {
		    handle_error( Err::index_out_of_bounds, i, j, nrows_, ncols_ );
		    i = j = 0;
		  }
		  return this->operator()(i,j);
		}

		// and so on...
		}; 

したがって、Matrixクラスはすべてのポリシーを機能クラスにまとめます。 
ユーザーが作成できる

* 行列<Row_major、ヒープ、例外>または
* 必要に応じてMatrix <Lower_triangular、Pool_allocation、Restart>。

Matrixクラスは、それから派生するのではなく、Access、Alloc、およびErrがMatrixのデータメンバーとして存在する場所に記述することができます。
この技術には、多数の転送機能を作成する（および維持する）必要性および性能特性が劣るなどのいくつかの欠点があります。


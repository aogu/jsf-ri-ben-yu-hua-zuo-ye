このドキュメントの目的は、安全で信頼性が高く、テスト可能で保守可能なコードにつながる優れたプログラミングスタイルと実績のあるプログラミング方法を採用できるC++プログラマーに方向性と指針を提供することです。
結果的に、この文書に含まれる規則は、航空機のC++開発に必要とされ、非航空機のC++開発に推奨されています。
上記のように、航空機(Air Vehicle/AV)コードの一部はC++で開発されます。
C++は、従来のCプログラミング技術との互換性を保ちながら、データ抽象化、オブジェクト指向プログラミング、汎用プログラミングをサポートするように設計されています。
このため、AVコーディング標準では次の点に重点を置いています。

1. 自動車産業ソフトウェア信頼性協会（MISRA）車両ベースのソフトウェアにおけるC言語の使用のためのガイドライン
2. 車載システムの安全重視なC言語コーディング標準
3. C++言語固有のガイドラインと標準

MISRAガイドラインは、特に安全面を含むシステムで使用するために作成されています。
このガイドラインは、潜在的に危険なC言語の機能に対処し、これらの落とし穴を回避するプログラミングルールを提供しています。
MISRA CサブセットをベースにしたCの車両システムセーフティクリティカルコーディングスタンダードは、車両システムの安全性が重要なアプリケーションに均一に適用される、より包括的な言語制限を提供します。
AVコーディング標準は、安全上重要な環境における適切な使用C++言語機能（例えば、継承、テンプレート、名前空間など）に固有の追加のルールセットを用いて、前の2つの文書の関連部分を構築する。
全体として、ルールセットによって具現化された哲学は、基本的にCとの関係でC++の哲学を拡張したものです。
すなわち、「安全でない」設備に「より安全な」代替手段を提供することによって、低レベルの特徴に関する既知の問題が回避される。
本質的には、プログラムはスーパーセットの「より安全な」サブセットに書き込まれます。


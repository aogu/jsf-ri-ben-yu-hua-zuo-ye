## AVルール36
可能であれば、ソースファイルの不必要な再コンパイルを排除する必要があります。
次の例では、各ソースファイルには、実際に必要なものがどれかを特定せずにすべてのヘッダーファイルが含まれています。

例：
実際に必要なファイルに関係なく、すべてのヘッダーファイルが3つのソースファイルに含まれています。
これにはいくつかの問題があります。
1. 
コンパイル範囲を制限できない。
つまり、1つのヘッダーファイルを変更すると、各ソースファイルを再コンパイルする（結果として再テストする）ことを意味します。
2. 
不必要に長いコンパイル時間。 不要なヘッダーファイルを繰り返しコンパイルすると、コンパイル時間が大幅に増加します。

File 1
		#include <header1.h>
		#include <header2.h> // Incorrect: unneeded
		#include <header3.h> // Incorrect: unneeded
		...                  // Source for file 1

File 2
		#include <header1.h> // Incorrect: unneeded
		#include <header2.h>
		#include <header3.h> // Incorrect: unneeded
		...                  // Source for file 2

File 3
		#include <header1.h> // Incorrect: unneeded
		#include <header2.h> // Incorrect: unneeded
		#include <header3.h>
		...                  // Source for file 3 


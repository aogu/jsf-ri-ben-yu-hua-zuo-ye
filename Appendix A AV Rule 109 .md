## AVルール109
次の例では、Squareは2つの関数area（）とmorph（）を宣言しています。
設計者は、比較的簡単なメソッド領域（）をインライン化したいので、クラス指定内で定義されます。
対照的に、複雑なメソッドmorph（）をインライン化するつもりはありません。
したがって、メソッド宣言だけが含まれています。

		class Square : public Shape
		{
		  public:
		    float32 area()
		    {
		      return length*width;
		    }               // area() will be inlined since it is defined
		                    // in the class specification.
		
		  morph (Shape &s); // morph() is not intended to be inlined so its
		};                  // implementation is contained in a separate file. 


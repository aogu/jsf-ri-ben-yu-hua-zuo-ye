## AVルール40
AVルール40は、1つの定義ルール（ODR）をサポートすることを意図している。
つまり、プログラム内の各エンティティに対して1つの定義のみが存在する可能性があります。
したがって、型の宣言（その定義に含まれる）を単一のヘッダーファイルに配置することにより、重複する定義が同一になります。

例A：
アプリケーション全体（つまり.cppファイル内）でタイプの定義を散布すると、一意でない定義（ODR違反）の可能性が高くなります。

s.cpp
---
		class S    // Bad: S declared in .cpp file.
		{          // S could be declared differently in a
		  int32 x; // separate .cpp file
		  char y;
		};

例B：
2つの異なるヘッダーファイルにSの定義を配置すると、一意でない定義（つまり、ODRの違反）が発生する可能性があります。

s.h
---
		class S
		{
		  int32 x;
		  char y;
		};

y.h
---
		class S // Bad: S multiply defined in two different header files.
		{
		  int32 x;
		  int32 y;
		};


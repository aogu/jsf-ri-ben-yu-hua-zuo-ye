## AVルール83
クラスには、継承階層内に存在するだけでなく、多くのデータメンバーが含まれる場合があります。
したがって、代入演算子は、次の例のように、クラス不変式に影響を及ぼす基本クラスを含むすべてのメンバーを割り当てる必要があります。

注：
operator =（）の定義は、この規則の説明を簡略化するためにクラス宣言に含まれています。
それは "クラス宣言の関数定義がない"ルールを破る。

		class Base
		{
		  public:
		    Base (int32 x) : base_member (x) {}

		    Base &operator=(const Base& rhs)
		    {
		      if (this != &rhs)                // Check for self assignment before continuing.
		      {
		        base_member = rhs.base_member; // Assign members (only one member in class).
		      }
		      else
		      {
		      }
		      return *this;
		    }

		  private:
		    int32 base_member;
		};

		class Derived : public Base
		{
		  public:
		    Derived (int32 x, int32 y, int32 z) : Base (x),
		                                          derived_member_1 (y),
		                                          derived_member_2 (z) {}

		    Derived& operator=(const Derived& rhs)
		    {
		      if (this != &rhs)                          // Check for self-assignment
		      {
		        Base::operator=(rhs);                    // Copy base class elements.
		        derived_member_1 = rhs.derived_member_1; // Assign all members of derived class
		        derived_member_2 = rhs.derived_member_2;
		      }
		      else
		      {
		      }

		      return *this;
		    } 

		  private:
		    int32 derived_member_1;
		    int32 derived_member_2;
		}; 


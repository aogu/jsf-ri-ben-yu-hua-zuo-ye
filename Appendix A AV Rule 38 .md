## AVルール38
ポインタまたは参照によってのみ参照されるクラスのヘッダーファイルはインクルードする必要はありません。
そうすることで、クラス間の結合が増加し、コンパイルの依存性が増し、メンテナンスの労力が増えます。
問題のクラス（フォワードヘッダによって提供される）の前方宣言は、実装の依存関係、メンテナンス作業およびコンパイル時間を制限するために使用できます。

例A：
この例では、不必要に、オペレータインタフェースに追加の依存関係を作成するヘッダファイルが含まれています。

Operator.h
		#include <LM_string.h> // Incorrect: creates unnecessary dependency
		#include <Date.h>      // Incorrect: creates unnecessary dependency
		#include <Record.h>    // Incorrect: creates unnecessary dependency
		class Operator
		{
		public:
		  Operator (const LM_string &name,
		  const Date &birthday,
		  const Record &flying_record);
		  LM_string get_name () const;
		  int32 get_age () const;
		  Record get_record () const;
		  ...
		private:
		  Operator_impl *impl;
		};

例B：
例Aとは対照的に、例Bでは、Forwardヘッダーを使用して、Operatorによって使用される実装クラスを宣言します。
したがって、オペレータインタフェースは、実装クラスのいずれにも依存しません。

Operator.h The forward headers only contain declarations.
		#include <LM_string_fwd.h>
		#include <Date_fwd.h>
		#include <Record_fwd.h>
		#include <OperatorImpl.h>
		class Operator
		{
		public:
		  Operator (const LM_string &name,
		  const Date & birthday,
		  const Record &flying_record);
		  LM_string get_name() const;
		  int32 get_age () const;
		  record get_record () const; 
		...
		private:
		  Operator_impl *impl;
		};

Operator.cc
		#include <Operator.h>
		#include <Operator_impl.h> // Contains implementation details of the Operator object.
		...
		int32 Operator::get_age()
		{
		  impl->get_age();
		}


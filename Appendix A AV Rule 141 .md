## AVルール141

例A：
型の定義で列挙型を宣言すると、以下に示すように、可読性の問題や名前のないデータ型につながる可能性があります。

		enum // Don’t do this: Creates an unnamed data type.
		{
		  up,
		  down
		} direction;

		enum i { in, out } i; // Don’t do this: Difficult to read. 

例B：
以下に示すように、宣言と定義の分離が優先されます。
これには、同じ型の他の変数を作成するメカニズムと型キャスト機能を提供するデータ型の名前が必要であることに注意してください。

		enum XYZ_direction
		{
		  up,
		  down
		};

		XYZ_direction direction; 

例C：
名前のない列挙を正当に使用することは、クラス宣言内に記号定数を定義することです。

		class X
		{
		  enum
		  {
		    max_length = 100,
		    max_time = 73
		  }; // Defines symbolic constants for the class
		  ...
		};

例D：
次の宣言はこの規則の下で禁じられていないことに注意してください。

		int32 i=0;
		pair<float32,int32> p;


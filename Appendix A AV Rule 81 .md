## AVルール81
自己割当は、割当担当者が適切に処理する必要があります。
例Aは潜在的な問題を示していますが、例Bは許容できるアプローチを示しています。

例A：
すべての場合に自己割り当てを確認する必要はありませんが、次の例は、適切なコンテキストを示しています。

		Base &operator= (const Base &rhs)
		{
		  release_handle (my_handle); // Error: the resource referenced by myHandle is
		  my_handle = rhs.myHandle;   // erroneously released in the self-assignment case.
		  return *this;
		} 

例B：
自己割り当てを処理する1つの方法は、以下に示すように、さらに処理を続ける前に自己割り当てをチェックすることです。

		Base &operator= (const Base& rhs)
		{
		  if (this != &rhs)            // Check for self assignment before continuing.
		  {
		    release_handle(my_handle); // Release resource.
		    my_handle = rhs.my_handle; // Assign members (only one member in class).
		  }
		  else
		  {
		  }
		  return *this;
		} 


## AVルール193
break文を使用してswitch文に空でないcase節を終了すると、制御が後続の文まで落ちないようにすることで、混乱を招く可能性のある動作を排除します。
以下の例では、プライマリとセカンダリの色が同様に扱われるため、各グループ内でbreakステートメントは不要です。
ただし、このコードセグメントが意図したとおりに動作するには、すべての空でないcase節をbreak文で終了する必要があります。

注意：
最後のデフォルト句を省略すると、すべての列挙値がswitch文でテストされていない場合、コンパイラは警告を出すことができます。

		switch (value)
		{
		  case red :                              // empty since primary_color() should be called
		  case green :                            // empty since primary_color() should be called
		  case blue : primary_color (value);
		    break;                                // Must break to end primary color handling
		  case cyan :
		  case magenta :
		  case yellow : secondary_color (value);
		    break;                                // Must break to end secondary color handling
		  case black : black (value);
		    break;
		  case white : white (value);
		    break;
		}

